=====================
Postfix configuration
=====================
This page mentions all Postfix configuration parameters, which have to be
modified and/or added in/to the Postfix :file:`main.cf`.

main.cf
-------
Add or replace the following configuration parameters in the global Postfix
configuration file.
The Postfix PostgreSQL client configuration files (:file:`pgsql-{*}.cf`)
mentioned below will be created when vmm will be installed.

.. code-block:: text

 sql      = pgsql:${config_directory}/
 proxysql = proxy:${sql}

 # relocated users from the database
 #relocated_maps = ${proxysql}pgsql-relocated_maps.cf
 
 # transport settings from our database
 transport_maps = ${proxysql}pgsql-transport_maps.cf
 
 # virtual domains, mailboxes and aliases
 virtual_mailbox_domains = ${proxysql}pgsql-virtual_mailbox_domains.cf
 virtual_alias_maps = ${proxysql}pgsql-virtual_alias_maps.cf
 virtual_minimum_uid = 70000
 virtual_uid_maps = ${sql}pgsql-virtual_uid_maps.cf
 virtual_gid_maps = ${sql}pgsql-virtual_gid_maps.cf
 virtual_mailbox_base = /
 virtual_mailbox_maps = ${proxysql}pgsql-virtual_mailbox_maps.cf
 
 # dovecot lmtp (requires Dovecot ≧ v2.0.0)
 virtual_transport = lmtp:unix:private/dovecot-lmtp
 
 # dovecot SASL
 smtpd_sasl_type = dovecot
 smtpd_sasl_path = private/dovecot-auth
 smtpd_sasl_auth_enable = yes
 # Keep smtpd_sasl_local_domain identical to Dovecot's auth_default_realm:
 # empty. Both are empty by default. Let it commented out.
 # Read more at: http://wiki2.dovecot.org/Authentication/Mechanisms/DigestMD5
 #smtpd_sasl_local_domain =
 smtpd_sasl_security_options = noplaintext, noanonymous
 #broken_sasl_auth_clients = yes

 smtpd_recipient_restrictions =
  permit_mynetworks
  permit_sasl_authenticated
  reject_unauth_destination

Using Dovecot's LMTP
--------------------
In order to use Dovecot's LMTP_ as `virtual_transport` with Postfix make sure
you have have:

 ‣ `lmtp` in your `protocols` setting (:ref:`dovecot2.conf`)
 ‣ configured a `lmtp` listener: `service lmtp {}`
   (:ref:`conf-d-10-master.conf`)
 ‣ set `lmtp:unix:private/dovecot-lmtp` as the `domain.transport` in your
   :file:`vmm.cfg`.

If you want to switch the transport of existing domains and/or accounts, you
can use the subcommands :ref:`domaintransport` and :ref:`usertransport`.

.. include:: ../ext_references.rst

.. External references
.. _COPYING: https://bitbucket.org/pvo/vmm/src/master/COPYING
.. _Client Authentication: \
 https://doc.dovecot.org/configuration_manual/authentication/
.. _Dovecot: https://dovecot.org/
.. _GID: https://en.wikipedia.org/wiki/Group_identifier_%28Unix%29
.. _GPG: https://en.wikipedia.org/wiki/GNU_Privacy_Guard
.. _IDN: https://en.wikipedia.org/wiki/Internationalized_domain_name
.. _IDN ccTLDs: \
 https://en.wikipedia.org/wiki/Internationalized_country_code_top-level_domain
.. _LMTP: https://doc.dovecot.org/configuration_manual/protocols/lmtp_server/
.. _Maildir: https://doc.dovecot.org/admin_manual/mailbox_formats/maildir
.. _MainConfig: https://doc.dovecot.org/configuration_manual/config_file/
.. _Git: https://git-scm.com/
.. _PGP: https://en.wikipedia.org/wiki/Pretty_Good_Privacy
.. _Postfix: http://www.postfix.org/
.. _PostgreSQL: https://www.postgresql.org/
.. _Psycopg: https://www.psycopg.org/
.. _Python: https://www.python.org/
.. _SourceForge: https://sourceforge.net/
.. _UID: https://en.wikipedia.org/wiki/User_identifier_%28Unix%29
.. _Upgrading: https://doc.dovecot.org/installation_guide/upgrading/
.. _dbox: https://doc.dovecot.org/admin_manual/mailbox_formats/dbox/
.. _download page: https://sourceforge.net/projects/vmm/files/
.. _libera.chat: https://libera.chat/
.. _integrity: https://en.wikipedia.org/wiki/Data_integrity
.. _issue tracker: https://bitbucket.org/pvo/vmm/issues
.. _passdb: https://doc.dovecot.org/configuration_manual/authentication/password_databases_passdb/
.. _pg_dump: https://www.postgresql.org/docs/current/static/app-pgdump.html
.. _pg_hba.conf: \
 https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html
.. _pgsql_table(5): http://www.postfix.org/pgsql_table.5.html
.. _relocated: http://www.postfix.org/relocated.5.html
.. _transport: http://www.postfix.org/transport.5.html
.. _userdb: https://doc.dovecot.org/configuration_manual/authentication/user_databases_userdb/
.. _virtual_alias_expansion_limit: \
 http://www.postfix.org/postconf.5.html#virtual_alias_expansion_limit
.. _vmm repository: https://bitbucket.org/pvo/vmm.git
.. _vmm-users: https://lists.sourceforge.net/lists/listinfo/vmm-users
.. _vmm: irc://irc.libera.chat/vmm

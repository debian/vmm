.. |curr_vers_rel_date| replace:: The most current version of vmm is 0.7.0,
 released on 07 November 2021.
.. |rel_hist| replace:: Older releases are listed in the :doc:`release_history`.


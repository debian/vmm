Source: vmm
Section: mail
Priority: optional
Maintainer: martin f. krafft <madduck@debian.org>
Build-Depends: debhelper (>= 10), dh-python, python3, python3-setuptools, python3-sphinx
Standards-Version: 3.9.8
Rules-Requires-Root: no
Homepage: http://vmm.localdomain.org/
Vcs-Git: https://salsa.debian.org/debian/vmm.git
Vcs-Browser: https://salsa.debian.org/debian/vmm

Package: vmm
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, dovecot-core (>= 2)
Recommends: postfix, postfix-pgsql, dovecot-lmtpd (>= 2), dovecot-pgsql (>= 2), postgresql-client, python3-psycopg2
Description: manage mail domains/accounts/aliases for Dovecot and Postfix
 Virtual Mail Manager is a command-line tool for administrators/postmasters to
 manage (alias-)domains, accounts, aliases and relocated users. It is designed
 for Dovecot and Postfix with a PostgreSQL backend.

Package: vmm-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Documentation for the Virtual Mail Manager
 Virtual Mail Manager is a command-line tool for administrators/postmasters to
 manage (alias-)domains, accounts, aliases and relocated users. It is designed
 for Dovecot and Postfix with a PostgreSQL backend.
 .
 This package provides the documentation for vmm, as well as the
 VirtualMailManager API.
